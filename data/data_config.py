class global_var:
	Id = '0'
	url = '1'
	run = '2'
	request_way = '3'
	expect = '5'
	result = '6'

def get_id():
	return global_var.Id

def get_url():
	return global_var.url

def get_run():
	return global_var.run

def get_run_way():
	return global_var.request_way

def get_expect():
	return global_var.expect

def get_result():
	return global_var.result
