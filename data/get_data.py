from util.operation_excel import OperationExcel
import data_config
class GetData:
	def __init__(self):
		self.opera_excel = OperationExcel()

	# check number of test cases
	def get_case_lines(self):
		return self.opera_excel.get_lines()

	# check whether the test will be run
	def get_is_run(self,row):
		flag = None
		col = int(data_config.get_run())
		run_model = self.opera_excel.get_cell_value(row,col)
		if run_model == 'Y':
			flag = True
		else:
			flag = False
		return flag

	# access request method
	def get_request_method(self,row):
		col = int(data_config.get_run_way())
		request_method = self.opera_excel.get_cell_value(row,col)
		return request_method

	# access url
	def get_request_url(self,row):
		col = int(data_config.get_url())
		url = self.opera_excel.get_cell_value(row,col)
		return url

	# access expected results
	def get_expect_data(self,row):
		col = int(data_config.get_expect())
		expect = self.opera_excel.get_cell_value(row,col)
		if expect == '':
			return None
		return expect

	# write result into excel
	def write_result(self,row,value):
		col = int(data_config.get_result())
		self.opera_excel.write_value(row,col,value)


