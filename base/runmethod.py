import requests
import json
class RunMethod:
	def post_main(self,url,data,header=None):
		res = None
		if header !=None:
			res = requests.post(url=url,data=data,headers=header)
		else:
			res = requests.post(url=url,data=data)
		return res.json()

	def get_main(self,url,data=None,header=None):
		res = None
		if header !=None:
			res = requests.get(url=url,data=data,headers=header,verify=False)
		else:
			requests.packages.urllib3.disable_warnings()
			res = requests.get(url=url,data=data,verify=False)
		if res.headers.get('content-type') == 'application/json;charset=utf-8':
			return res.json()
		else:
			return 'Status:' + str(res.status_code)
	def run_main(self,method,url,data=None,header=None):
		res = None
		if method == 'Get':
			res = self.get_main(url, data, header)
		else:
			res = self.post_main(url, data, header)
		return json.dumps(res,ensure_ascii=False)
		#return json.dumps(res,ensure_ascii=False,sort_keys=True,indent=2)
