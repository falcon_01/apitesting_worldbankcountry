import sys
import unittest
sys.path.append(r"C:\APITesting_WorldBankCountry\data")
from base.runmethod import RunMethod
from data.get_data import GetData
from util.common_util import CommonUtil


class TestMethod(unittest.TestCase):

    def setUp(self):
        super().__init__()
        self.run_method = RunMethod()
        self.data = GetData()
        self.com_util = CommonUtil()

    def test_01(self):
        is_run = self.data.get_is_run(1)
        if is_run:
            url = self.data.get_request_url(1)
            method = self.data.get_request_method(1)
            expect = self.data.get_expect_data(1)
            res = self.run_method.run_main(method, url)
            if self.com_util.is_equal_json(expect, res):
                self.data.write_result(1, 'pass')
            else:
                self.data.write_result(1, 'fail')
            self.assertEqual(expect, res)

    def test_02(self):
        is_run = self.data.get_is_run(2)
        if is_run:
            url = self.data.get_request_url(2)
            method = self.data.get_request_method(2)
            expect = self.data.get_expect_data(2)
            res = self.run_method.run_main(method, url)
            if self.com_util.is_equal_json(expect, res):
                self.data.write_result(2, 'pass')
            else:
                self.data.write_result(2, 'fail')
            self.assertEqual(expect, res)

    def test_03(self):
        is_run = self.data.get_is_run(3)
        if is_run:
            url = self.data.get_request_url(3)
            method = self.data.get_request_method(3)
            expect = self.data.get_expect_data(3)
            res = self.run_method.run_main(method, url)
            if self.com_util.is_equal_json(expect, res):
                self.data.write_result(3, 'pass')
            else:
                self.data.write_result(3, 'fail')
            self.assertEqual(expect, res)

    def test_04(self):
        is_run = self.data.get_is_run(4)
        if is_run:
            url = self.data.get_request_url(4)
            method = self.data.get_request_method(4)
            expect = self.data.get_expect_data(4)
            res = self.run_method.run_main(method, url)
            if self.com_util.is_equal_json(expect, res):
                self.data.write_result(4, 'pass')
            else:
                self.data.write_result(4, 'fail')
            self.assertEqual(expect, res)

    def test_05(self):
        is_run = self.data.get_is_run(5)
        if is_run:
            url = self.data.get_request_url(5)
            method = self.data.get_request_method(5)
            expect = self.data.get_expect_data(5)
            res = self.run_method.run_main(method, url)
            if self.com_util.is_equal_json(expect, res):
                self.data.write_result(5, 'pass')
            else:
                self.data.write_result(5, 'fail')
            self.assertEqual(expect, res)

    def test_06(self):
        is_run = self.data.get_is_run(6)
        if is_run:
            url = self.data.get_request_url(6)
            method = self.data.get_request_method(6)
            expect = self.data.get_expect_data(6)
            res = self.run_method.run_main(method, url)
            if self.com_util.is_equal_json(expect, res):
                self.data.write_result(6, 'pass')
            else:
                self.data.write_result(6, 'fail')
            self.assertEqual(expect, res)

    def test_07(self):
        is_run = self.data.get_is_run(7)
        if is_run:
            url = self.data.get_request_url(7)
            method = self.data.get_request_method(7)
            expect = self.data.get_expect_data(7)
            res = self.run_method.run_main(method, url)
            if self.com_util.is_equal_json(expect, res):
                self.data.write_result(7, 'pass')
            else:
                self.data.write_result(7, 'fail')
            self.assertEqual(expect, res)
