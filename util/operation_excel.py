import xlrd
from openpyxl import load_workbook


class OperationExcel:
    def __init__(self, file_name=None, sheet_id=None):
        if file_name:
            self.file_name = file_name
            self.sheet_id = sheet_id
        else:
            self.file_name = '../test_case/TestCases.xlsx'
            self.sheet_id = 0
        self.data = self.get_data()

    # access sheet contents
    def get_data(self):
        data = xlrd.open_workbook(self.file_name)
        tables = data.sheets()[self.sheet_id]
        return tables

    # access rows of excel cell
    def get_lines(self):
        tables = self.data
        return tables.nrows

    # access content of an excel cell
    def get_cell_value(self, row, col):
        return self.data.cell_value(row, col)

    # write data into excel
    def write_value(self, row, col, value):
        '''
		write in excel data
		row,col,value
		'''
        wb = load_workbook(self.file_name)
        ws = wb.active
        ws['G' + str(row + 1)] = value
        wb.save(self.file_name)

    # access row's content by case_id
    def get_rows_data(self, case_id):
        row_num = self.get_row_num(case_id)
        rows_data = self.get_row_values(row_num)
        return rows_data

    # access row num by case_id
    def get_row_num(self, case_id):
        num = 0
        clols_data = self.get_cols_data()
        for col_data in clols_data:
            if case_id in col_data:
                return num
            num = num + 1

    # access row's content by row's num
    def get_row_values(self, row):
        tables = self.data
        row_data = tables.row_values(row)
        return row_data

    # access content of a column
    def get_cols_data(self, col_id=None):
        if col_id != None:
            cols = self.data.col_values(col_id)
        else:
            cols = self.data.col_values(0)
        return cols


if __name__ == '__main__':
    opers = OperationExcel()
    print(opers.get_cell_value(1, 2))
