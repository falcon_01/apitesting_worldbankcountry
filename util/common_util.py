import json
from filecmp import cmp

from idna import unicode


class CommonUtil:
	def is_contain(self,str_one,str_two):
		flag = None
		if isinstance(str_one,unicode):
			str_one = str_one.encode('unicode-escape').decode('string_escape')
		return cmp(str_one,str_two)


	def is_equal_json(self, a, b):
		a, b = json.dumps(a, sort_keys=True), json.dumps(b, sort_keys=True)
		if a == b:
			return True
		else:
			return False

