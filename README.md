Introduction: 
	This API automation testing project is to verify the basic functions of WorldBank's Country API, it is developed based on Python+unittest+requests+HTMLTestRunner. 
	

Prerequisites:
	Python 3.8
	Need to have below packages installed if they don't exist:
		requests
		xlrd==1.2.0
		openpyxl


Instructions on running the tests:
	Clone this project to the root of "C:" drive on your local machine
	Open the project using IDE like PyCharm
	Navigate to the "run_all.py" under "main" package
	Execute the "run_all.py"
	Test results will be saved to "TestReport.html" generated under the "report" package

Some information about the working processes:
	When starting the tests, the program will retrieve test data of different test cases from "TestCases.xlsx" under "test_case" package, then the program will test the corresponding api based on different test cases and basically compare the response information of json format with the expected data previously stored in the "TestCases.xlsx". The test result (pass/fail) will be saved back to the "TestCases.xlsx", and overall test results will be saved to "TestReport.html" generated under the "report" package. Test cases can be extended by modifying the "TestCases.xlsx" and creating additional tests within the project. 