import unittest
from base import HTMLTestRunner
from base.test_method import TestMethod


class RunTest:

    def run(self):
        file_path = '../report/TestReport.html'
        fp = open(file_path, 'wb')
        suite = unittest.TestSuite()
        suite.addTest(TestMethod('test_01'))
        suite.addTest(TestMethod('test_02'))
        suite.addTest(TestMethod('test_03'))
        suite.addTest(TestMethod('test_04'))
        suite.addTest(TestMethod('test_05'))
        suite.addTest(TestMethod('test_06'))
        suite.addTest(TestMethod('test_07'))
        runner = HTMLTestRunner.HTMLTestRunner(stream=fp, title='Test Report', description='Test Description')
        runner.run(suite)


if __name__ == '__main__':
    instance = RunTest()
    instance.run()

